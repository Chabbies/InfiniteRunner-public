# Preview:
![game_image](https://gitlab.com/Chabbies/InfiniteRunner-public/raw/master/game_unity.JPG)

# Instructions:
    1. <Spacebar> to jump.
    2. Eat fruits.
    3. Don't die.

**NOTE:** The code is hidden as it contains Asset Store components.